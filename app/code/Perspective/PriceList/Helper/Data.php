<?php


namespace Perspective\PriceList\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const PATH_PRICE_LIST_ENABLE = 'perspective_price_section_id/perspective_price_group_id/enable_price';
    const PATH_CUSTOM_FINAL_PRICE = 'perspective_price_section_id/perspective_price_group_id/final_price';
    const PATH_CUSTOM_SPECIAL_PRICE = 'perspective_price_section_id/perspective_price_group_id/special_price';
    const PATH_CUSTOM_BASE_PRICE = 'perspective_price_section_id/perspective_price_group_id/base_price';
    const PATH_CUSTOM_TIER_PRICE = 'perspective_price_section_id/perspective_price_group_id/tier_price';
    const PATH_CUSTOM_CATALOG_RULE_PRICE = 'perspective_price_section_id/perspective_price_group_id/catalog_rule_price';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    public function getEnableConfigPrice()
    {
        return  $this->getConfigValue(self::PATH_PRICE_LIST_ENABLE);
    }
    public function getEnableFinal()
    {
        return  $this->getConfigValue(self::PATH_CUSTOM_FINAL_PRICE);
    }
    public function getEnableSpecial()
    {
        return  $this->getConfigValue(self::PATH_CUSTOM_SPECIAL_PRICE);
    }
    public function getEnableBase()
    {
        return  $this->getConfigValue(self::PATH_CUSTOM_BASE_PRICE);
    }
    public function getEnableTier()
    {
        return  $this->getConfigValue(self::PATH_CUSTOM_TIER_PRICE);
    }
    public function getEnableCatRule()
    {
        return  $this->getConfigValue(self::PATH_CUSTOM_CATALOG_RULE_PRICE);
    }
}
