<?php


namespace Perspective\PriceList\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Model\ResourceModel\Customer;
use Magento\Framework\View\Element\BlockInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Perspective\PriceList\Helper\Data;
use Magento\Catalog\Model\Product;

class CustomPrice extends Template implements BlockInterface
{
    /**
     * @var Data
     */
    private $dataValue;
    /**
     * @var Product
     */
    private $productModel;
    /**
     * @var Customer
     */

    /**
     * CustomPrice constructor.
     * @param Context $context
     * @param Data $dataValue
     * @param Product $productModel
     */
    public function __construct(
        Context $context,
        Data $dataValue,
        Product $productModel,
        ProductRepositoryInterface $productInterfaceFactory
    ) {
        $this->productInterfaceFactory = $productInterfaceFactory;
        $this->dataValue = $dataValue;
        $this->productModel = $productModel;
        parent::__construct($context);
    }

    public function getCustomProduct()
    {
        $productId = $this->getRequest()->getParam('id');
        return $this->productModel->load($productId);
    }

    public function isEnablePrice()
    {
        $this->dataValue->getEnableFinal();
        $this->dataValue->getEnableSpecial();
        $this->dataValue->getEnableBase();
        $this->dataValue->getEnableTier();
        $this->dataValue->getEnableCatRule();
    }

    public function getCustomFinalPrice($product)
    {
        $finalPrice = $this->productModel->getPriceInfo($product)->getPrice('final_price')->getValue();
        if ($this->dataValue->getEnableFinal() === '1' && $finalPrice) {
            return 'Final price = ' . $finalPrice . ' $';
        }
        return 'Final price is empty';
    }

    public function getCustomSpecialPrice($product)
    {
        $specialPrice = $this->productModel->getPriceInfo($product)->getPrice('special_price')->getValue();
        if ($this->dataValue->getEnableSpecial() === '1' && $specialPrice) {
            return 'Special price = ' . $specialPrice . ' $';
        }
        return 'Special price is empty';
    }

    public function getCustomBasePrice($product)
    {
        $basePrice = $this->productModel->getPriceInfo($product)->getPrice('base_price')->getValue();
        if ($this->dataValue->getEnableBase() === '1' && $basePrice) {
            return 'Base Prise = ' . $basePrice . ' $';
        }
        return 'Base price is empty';
    }

    public function getCustomTierPrice($product)
    {
        $tierPrice = $this->productModel->getPriceInfo($product)->getPrice('tier_price')->getValue();
        if ($this->dataValue->getEnableTier() === '1' && $tierPrice) {
            return 'Tier Prise = ' . $tierPrice . ' $';
        }
        return 'Tier price is empty';
    }

    public function getCatalogRulePrice($product)
    {
        $catalogRulePrice = $this->productModel->getPriceInfo($product)->getPrice('catalog_rule_price')->getValue();
        if ($this->dataValue->getEnableCatRule() === '1' && $catalogRulePrice) {
            return 'Catalog Rule Prise = ' . $catalogRulePrice . ' $';
        }
        return 'Catalog rule price is empty';
    }

    public function showPrices()
    {
        $prices = [];
        if ($this->dataValue->getEnableConfigPrice()) {
            $product = $this->getCustomProduct();
            $prices['final_price'] = $this->getCustomFinalPrice($product);
            $prices['special_price'] = $this->getCustomSpecialPrice($product);
            $prices['base_price'] = $this->getCustomBasePrice($product);
            $prices['tier_price'] = $this->getCustomTierPrice($product);
            $prices['catalog_rule_price'] = $this->getCatalogRulePrice($product);
            return $prices;
        } else {
            return ['Module is disable'];
        }
    }
}
