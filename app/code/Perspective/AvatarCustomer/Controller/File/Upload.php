<?php

namespace Perspective\AvatarCustomer\Controller\File;

/**
 * Class Upload for upload file action
 */
class Upload extends AbstractUpload
{
    /**
     * @return mixed|string
     */
    public function getFileId()
    {
        return $this->_request->getParam('param_name', 'image');
    }
}
