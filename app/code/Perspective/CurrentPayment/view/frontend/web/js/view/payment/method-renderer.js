define(
    [
        'uiComponent',
        'Magento_Checkout/js/model/payment/renderer-list'
    ],
    function (
        Component,
        rendererList
    ) {
        'use strict';
        rendererList.push(
            {
                type: 'currentpayment',
                component: 'Perspective_CurrentPayment/js/view/payment/method-renderer/currentpayment'
            }
        );
        return Component.extend({});
    }
);
