<?php


namespace Perspective\CurrentPayment\Model;

use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Quote\Api\Data\CartInterface;
use Perspective\CurrentShipping\Model\Shipping;

class Payment extends AbstractMethod
{
    protected $_code = 'currentpayment';

    public function isAvailable(CartInterface $quote = null)
    {
        $result = parent::isAvailable($quote);
        if ($result) {
            if ($quote) {
                $shippingMethod = $quote->getShippingAddress();

                if ($shippingMethod) {
                    if ($shippingMethod->getShippingMethod() !== Shipping::CODE  . "_" . Shipping::CODE) {
                        return false;
                    }
                }
            }
            return $result;
        }
        return false;
    }
}
