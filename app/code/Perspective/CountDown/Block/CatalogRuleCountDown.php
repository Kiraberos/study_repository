<?php


namespace Perspective\CountDown\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Url\EncoderInterface;
use Magento\Store\Model\StoreManagerInterface;
use Perspective\CountDown\Helper\RuleFromProduct;

class CatalogRuleCountDown extends View
{
    /**
     * @var DateTimeFactory
     */
    private $dateTimeFactory;
    /**
     * @var Session
     */
    private $_customerSession;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var RuleFromProduct
     */
    private $ruleFromProduct;
    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * SpecialPriceCountDown constructor.
     * @param Context $context
     * @param EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param StringUtils $string
     * @param Product $productHelper
     * @param ConfigInterface $productTypeConfig
     * @param FormatInterface $localeFormat
     * @param Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param DateTimeFactory $dateTimeFactory
     * @param DateTime $dateTime
     * @param StoreManagerInterface $storeManager
     * @param RuleFromProduct $ruleFromProduct
     * @param array $data
     */
    public function __construct(
        Context $context,
        EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        StringUtils $string,
        Product $productHelper,
        ConfigInterface $productTypeConfig,
        FormatInterface $localeFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        DateTimeFactory $dateTimeFactory,
        DateTime $dateTime,
        StoreManagerInterface $storeManager,
        RuleFromProduct $ruleFromProduct,
        array $data = []
    ) {
        $this->dateTime = $dateTime;
        $this->ruleFromProduct = $ruleFromProduct;
        $this->storeManager = $storeManager;
        $this->dateTimeFactory = $dateTimeFactory;
        $this->_customerSession = $customerSession;
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );
    }

    public function getAllCatalogRule(): array
    {
        $productId = $this->getProduct()->getId();
        $storeId = $this->getProduct()->getStoreId();
        return $this->ruleFromProduct->getRuleFromProduct($productId, $storeId);
    }

    public function getProductSpecialDate()
    {
        $specialDate = $this->getProduct()->getSpecialToDate();
        if ($specialDate) {
            return strtotime($specialDate);
        }
        return 0;
    }

    public function isValidateDate($date): bool
    {
        $currentTime = $this->dateTime->gmtDate();
        $currentTimeUnix = strtotime($currentTime);
        return $currentTimeUnix < $date;
    }

    public function getFasterCatalogRule(): int
    {
        $catalogRuleTimes = $this->getAllCatalogRule();
        $arrToTimes = [];
        foreach ($catalogRuleTimes as $ruleTime) {
            $arrToTimes[] = $ruleTime['to_time'];
        }
        sort($arrToTimes);
        foreach ($arrToTimes as $arrToTime) {
            if ($this->isValidateDate($arrToTime)) {
                return $arrToTime;
            }
        }
        return 0;
    }

    public function getFasterBtwCaRulAndSpec(): array
    {
        $catalogTime = $this->getFasterCatalogRule();
        $specialTime = $this->getProductSpecialDate();
        $result = [];
        $result['message'] = 'Empty';
        $result['status'] = false;
        if ($specialTime == 0 || $catalogTime == 0) {
            $result = $this->checkDataOnZero($result, $specialTime, $catalogTime);
        } elseif ($catalogTime > $specialTime) {
            $result = $this->setResultData('Special Time', $specialTime, $result);
        } else {
            $result = $this->setResultData('Catalog Time', $catalogTime, $result);
        }
        return $result;
    }

    /**
     * @param $message
     * @param $data
     * @param $result
     */
    public function setResultData($message, $data, $result)
    {
        $result['status'] = true;
        $result['message'] = $message;
        $result['timer'] = $data;
        return $result;
    }

    /**
     * @param $dataArrResult
     * @param $specialTime
     * @param $catalogTime
     * @return mixed
     */
    public function checkDataOnZero($dataArrResult, $specialTime, $catalogTime)
    {
        if ($specialTime === 0) {
            if ($catalogTime !== 0) {
                $dataArrResult['status'] = true;
                $dataArrResult['message'] = 'Catalog Time';
                $dataArrResult['timer'] = $catalogTime;
            }
        }
        if ($catalogTime === 0) {
            if ($specialTime !== 0) {
                $dataArrResult['status'] = true;
                $dataArrResult['message'] = 'Special Time';
                $dataArrResult['timer'] = $specialTime;
            }
        }
        return $dataArrResult;
    }

    public function resolveTime($date): string
    {
        return $this->dateTime->date('Y-m-d H:i:s', $date);
    }
}
