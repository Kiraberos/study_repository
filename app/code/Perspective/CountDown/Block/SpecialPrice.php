<?php


namespace Perspective\CountDown\Block;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Block\Product\View;
use Magento\Catalog\Helper\Product;
use Magento\Catalog\Model\ProductTypes\ConfigInterface;
use Magento\CatalogRule\Model\ResourceModel\RuleFactory;
use Magento\Customer\Model\Session;
use Magento\Framework\Locale\FormatInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\DateTime\DateTimeFactory;
use Magento\Framework\Stdlib\StringUtils;
use Magento\Framework\Url\EncoderInterface;
use Magento\Store\Model\StoreManagerInterface;

class SpecialPrice extends View
{
    /**
     * @var Context
     */
    private $context;
    /**
     * @var \Magento\Framework\Json\EncoderInterface
     */
    private $jsonEncoder;
    /**
     * @var Product
     */
    private $productHelper;
    /**
     * @var FormatInterface
     */
    private $localeFormat;
    /**
     * @var \DateTimeFactory
     */
    private $dateTimeFactory;
    /**
     * @var \DateTimeFactory
     */
    private $dateTimeFactoryPHP;
    private $storeManager;

    /**
     * Custom constructor.
     * @param \DateTimeFactory $dateTimeFactoryPHP
     * @param DateTimeFactory $dateTimeFactory
     * @param Context $context
     * @param EncoderInterface $urlEncoder
     * @param \Magento\Framework\Json\EncoderInterface $jsonEncoder
     * @param StringUtils $string
     * @param Product $productHelper
     * @param ConfigInterface $productTypeConfig
     * @param FormatInterface $localeFormat
     * @param Session $customerSession
     * @param ProductRepositoryInterface $productRepository
     * @param PriceCurrencyInterface $priceCurrency
     * @param StoreManagerInterface $storeManager
     * @param RuleFactory $rules
     * @param array $data
     */
    public function __construct(
        \DateTimeFactory $dateTimeFactoryPHP,
        DateTimeFactory $dateTimeFactory,
        Context $context,
        EncoderInterface $urlEncoder,
        \Magento\Framework\Json\EncoderInterface $jsonEncoder,
        StringUtils $string,
        Product $productHelper,
        ConfigInterface $productTypeConfig,
        FormatInterface $localeFormat,
        Session $customerSession,
        ProductRepositoryInterface $productRepository,
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager,
        RuleFactory $rules,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $urlEncoder,
            $jsonEncoder,
            $string,
            $productHelper,
            $productTypeConfig,
            $localeFormat,
            $customerSession,
            $productRepository,
            $priceCurrency,
            $data
        );

        $this->context = $context;
        $this->urlEncoder = $urlEncoder;
        $this->jsonEncoder = $jsonEncoder;
        $this->string = $string;
        $this->productHelper = $productHelper;
        $this->productTypeConfig = $productTypeConfig;
        $this->localeFormat = $localeFormat;
        $this->customerSession = $customerSession;
        $this->productRepository = $productRepository;
        $this->priceCurrency = $priceCurrency;

        $this->dateTimeFactory = $dateTimeFactory;
        $this->dateTimeFactoryPHP = $dateTimeFactoryPHP;
        $this->storeManager = $storeManager;
        $this->ruleFactory = $rules;
    }

    public function getProductData()
    {
        return $this->getProduct();
    }

    public function getIsSpecialPrice()
    {
        $productData = $this->getProductData();
        if ($productData->getSpecialPrice() &&
            $productData->getSpecialToDate()) {
            $currentDate = date('Y-m-d H:i:s');
            $endSpecialDate = $productData->getSpecialToDate();
            $differenceTime = strtotime($endSpecialDate) - strtotime($currentDate);
            $timestamp = $this->dateTimeFactoryPHP->create()->setTimestamp($differenceTime);
            $result = $timestamp->format('d H:i:s');
            return $result;

        }
        return "Countdown is empty";
    }
}
