<?php


namespace Perspective\CountDown\Helper;

use Magento\CatalogRule\Model\ResourceModel\Rule;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Http\Context;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Magento\Store\Model\StoreManagerInterface;

class RuleFromProduct
{
    /**
     * @var Rule
     */
    private $rule;
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var StoreManagerInterface
     */
    private $storeManager;
    /**
     * @var Session
     */
    private $session;
    private $dateTime;

    public function __construct(
        Session $session,
        StoreManagerInterface $storeManager,
        DateTime $dateTime,
        Context $httpSession,
        Session $customerSession,
        Rule $rule
    ) {
        $this->_customerSession = $customerSession;
        $this->rule = $rule;
        $this->httpSession = $httpSession;
        $this->dateTime = $dateTime;
        $this->storeManager = $storeManager;
        $this->session = $session;
    }

    public function getRuleFromProduct($productId, $storeId)
    {
        $store = $this->storeManager->getStore($storeId);
        $websiteId = $store->getWebsiteId();
        $date = $this->dateTime->gmtDate();
        $customerGroupId = $this->_customerSession->getCustomer()->getGroupId();
        $rules = $this->rule->getRulesFromProduct($date, $websiteId, $customerGroupId, $productId);

        return $rules;
    }
}
