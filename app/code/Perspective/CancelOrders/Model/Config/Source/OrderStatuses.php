<?php

namespace Perspective\CancelOrders\Model\Config\Source;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Sales\Model\ResourceModel\Order\Status\Collection;

class OrderStatuses  implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    private $orderCollection;

    public function __construct(
        Collection $orderCollection
    ) {
        $this->orderCollection=$orderCollection;
    }

    public function toOptionArray()
    {
        $orderStatus = $this->orderCollection->toOptionArray();

        foreach ($orderStatus as $orderValue)
        {
            $orderList = $orderValue['value'];
            if($orderList !== 'canceled' && $orderList !== 'closed'){
                $orderStatuses[] = ['value' => $orderList, 'label' => __($orderValue['label'])];
            }
        }
        return $orderStatuses;
    }
}
