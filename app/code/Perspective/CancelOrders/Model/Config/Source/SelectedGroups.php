<?php

namespace Perspective\CancelOrders\Model\Config\Source;

use Magento\Customer\Model\ResourceModel\Group\Collection;
use Magento\Framework\Data\OptionSourceInterface;

class SelectedGroups implements OptionSourceInterface
{
    /**
     * @var Collection
     */
    private $customerGroup;

    public function __construct(
        Collection $customerGroup
    ) {
        $this->customerGroup = $customerGroup;
    }
//
//    public function getCustomerGroups() {
//        $customerGroups = $this->customerGroup->toOptionArray();
//        return $customerGroups;
//    }

    public function toOptionArray()
    {
        $customerGroups = $this->customerGroup->toOptionArray();

        foreach ($customerGroups as $customerGroup)
        {
            if($customerGroup['value'] !='0'){
                $getGroupValues[] = ['value' => $customerGroup['value'], 'label'=>__($customerGroup['label'])];
            }
        }
        return $getGroupValues;
    }
}
