<?php

namespace Perspective\CancelOrders\Model;

use Magento\Framework\Model\AbstractModel;

/**
 * class OrderCancel
 */
class CancelOrder extends AbstractModel
{
    //constructor
    public function _construct()
    {
        $this->_init(ResourceModel\CancelOrder::class);
    }
}
