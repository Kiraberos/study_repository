<?php

namespace Perspective\CancelOrders\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

/**
 * class OrderCancel
 */
class CancelOrder extends AbstractDb
{
    //Initialising table.
    public function _construct()
    {
        $this->_Init('perspective_cancel_request', 'Id');
    }
}
