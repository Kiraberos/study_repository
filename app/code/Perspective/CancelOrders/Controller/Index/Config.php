<?php

namespace Perspective\CancelOrders\Controller\Index;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Perspective\CancelOrders\Helper\Data;
use Perspective\CancelOrders\Model\CancelOrderFactory;

class Config extends Action
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var CancelOrderFactory
     */
    protected $_orderCancel;

    protected $_objectManager;
    /**
     * @var CancelOrder
     */
    private $orderCreate;

    /**
     * @param Context $context
     * @param Data $helperData
     * @param CancelOrderFactory $orderCreate
     */
    public function __construct(
        Context $context,
        Data $helperData,
        CancelOrderFactory $orderCreate
    ) {
        $this->orderCreate = $orderCreate;
        $this->helperData = $helperData;
        return parent::__construct($context);
    }
    /**
     * Method to save request and send mail to admin.
     */
    public function execute()
    {
        $orderPost = $this->orderCreate->create();
        $data = $this->getRequest()->getPost();

        $orderPost->setData('order_cancel_reason', $data['reason']);
        $orderPost->setData('status', $data['status']);
        $orderPost->setData('state', $data['state']);
        $orderPost->setData('request_id', $data['entityId']);
        $orderPost->setData('customer_name', $this->helperData->getCustomerData()->getName());

        try {
            $orderPost->save();
            $this->messageManager->addSuccessMessage(__('Your order cancel request is sent.This may take some time.'));

        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage(
                __('Sorry request has been sent before. Please wait, this may take some time.')
            );
        }
    }
}
