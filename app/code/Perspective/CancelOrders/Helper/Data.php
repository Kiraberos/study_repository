<?php

namespace Perspective\CancelOrders\Helper;

use Magento\Customer\Model\SessionFactory;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH_CANCELORDER = 'Cancelorder/';
    const XML_PATH_MULTISELECT = 'cancel_order/general/enable';
    private $_customerSession;

    /**
     * @param $field
     * @param null $storeId
     * @return mixed
     */
    public function __construct(
        Context $context,
        SessionFactory $customerSession
    ) {
        $this->_customerSession = $customerSession->create();
        parent::__construct($context);
    }

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }

    /**
     * @param $code
     * @param null $storeId
     * @return mixed
     */
    public function getGeneralConfig($code, $storeId = null)
    {
        return $this->getConfigValue(self::XML_PATH_CANCELORDER . 'general/' . $code, $storeId);
    }

    public function getCustomerData()
    {
        if ($this->_customerSession->isLoggedIn()) {
            return $this->_customerSession->getCustomer();
        }
    }

    public function getEnableModule()
    {
        return $this->getConfigValue(self::XML_PATH_MULTISELECT);
    }
}
