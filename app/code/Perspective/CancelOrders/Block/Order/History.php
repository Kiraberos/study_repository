<?php
/**
 * Copyright © 2013-2017 Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Perspective\CancelOrders\Block\Order;

use Magento\Customer\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\Order\Config;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;

/**
 * Sales order history block
 */
class History extends \Magento\Sales\Block\Order\History
{
    /**
     * @var \Perspective\CancelOrders\Helper\Data
     */
    private $helper;

    /**
     * @param Context $context
     * @param CollectionFactory $orderCollectionFactory
     * @param Session $customerSession
     * @param Config $orderConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        CollectionFactory $orderCollectionFactory,
        Session $customerSession,
        Config $orderConfig,
        \Perspective\CancelOrders\Helper\Data $helper,
        array $data = []
    ) {
        $this->_session = $customerSession;
        $this->helper = $helper;
        parent::__construct($context, $orderCollectionFactory, $customerSession, $orderConfig, $data);
    }

    /**
     * @param object $order
     * @return string
     */
    public function getCancelUrl($order)
    {
        return $this->getUrl('sales/order/cancel', ['order_id' => $order->getId()]);
    }

    /**
     * @return bool
     */
    public function isEnabled()
    {
       return (bool)$this->helper->getEnableModule();
    }
}
