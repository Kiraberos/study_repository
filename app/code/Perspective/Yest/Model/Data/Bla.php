<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Model\Data;

use Perspective\Yest\Api\Data\BlaInterface;

class Bla extends \Magento\Framework\Api\AbstractExtensibleObject implements BlaInterface
{

    /**
     * Get bla_id
     * @return string|null
     */
    public function getBlaId()
    {
        return $this->_get(self::BLA_ID);
    }

    /**
     * Set bla_id
     * @param string $blaId
     * @return \Perspective\Yest\Api\Data\BlaInterface
     */
    public function setBlaId($blaId)
    {
        return $this->setData(self::BLA_ID, $blaId);
    }

    /**
     * Get Bla
     * @return string|null
     */
    public function getBla()
    {
        return $this->_get(self::BLA);
    }

    /**
     * Set Bla
     * @param string $bla
     * @return \Perspective\Yest\Api\Data\BlaInterface
     */
    public function setBla($bla)
    {
        return $this->setData(self::BLA, $bla);
    }

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Perspective\Yest\Api\Data\BlaExtensionInterface|null
     */
    public function getExtensionAttributes()
    {
        return $this->_getExtensionAttributes();
    }

    /**
     * Set an extension attributes object.
     * @param \Perspective\Yest\Api\Data\BlaExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Perspective\Yest\Api\Data\BlaExtensionInterface $extensionAttributes
    ) {
        return $this->_setExtensionAttributes($extensionAttributes);
    }
}

