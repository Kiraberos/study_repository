<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Model;

use Magento\Framework\Api\DataObjectHelper;
use Perspective\Yest\Api\Data\BlaInterface;
use Perspective\Yest\Api\Data\BlaInterfaceFactory;

class Bla extends \Magento\Framework\Model\AbstractModel
{

    protected $blaDataFactory;

    protected $dataObjectHelper;

    protected $_eventPrefix = 'perspective_yest_bla';

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param BlaInterfaceFactory $blaDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Perspective\Yest\Model\ResourceModel\Bla $resource
     * @param \Perspective\Yest\Model\ResourceModel\Bla\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        BlaInterfaceFactory $blaDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Perspective\Yest\Model\ResourceModel\Bla $resource,
        \Perspective\Yest\Model\ResourceModel\Bla\Collection $resourceCollection,
        array $data = []
    ) {
        $this->blaDataFactory = $blaDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve bla model with bla data
     * @return BlaInterface
     */
    public function getDataModel()
    {
        $blaData = $this->getData();
        
        $blaDataObject = $this->blaDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $blaDataObject,
            $blaData,
            BlaInterface::class
        );
        
        return $blaDataObject;
    }
}

