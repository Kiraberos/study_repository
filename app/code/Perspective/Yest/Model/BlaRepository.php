<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Store\Model\StoreManagerInterface;
use Perspective\Yest\Api\BlaRepositoryInterface;
use Perspective\Yest\Api\Data\BlaInterfaceFactory;
use Perspective\Yest\Api\Data\BlaSearchResultsInterfaceFactory;
use Perspective\Yest\Model\ResourceModel\Bla as ResourceBla;
use Perspective\Yest\Model\ResourceModel\Bla\CollectionFactory as BlaCollectionFactory;

class BlaRepository implements BlaRepositoryInterface
{

    protected $resource;

    protected $blaFactory;

    protected $blaCollectionFactory;

    protected $searchResultsFactory;

    protected $dataObjectHelper;

    protected $dataObjectProcessor;

    protected $dataBlaFactory;

    protected $extensionAttributesJoinProcessor;

    private $storeManager;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;

    /**
     * @param ResourceBla $resource
     * @param BlaFactory $blaFactory
     * @param BlaInterfaceFactory $dataBlaFactory
     * @param BlaCollectionFactory $blaCollectionFactory
     * @param BlaSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceBla $resource,
        BlaFactory $blaFactory,
        BlaInterfaceFactory $dataBlaFactory,
        BlaCollectionFactory $blaCollectionFactory,
        BlaSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->blaFactory = $blaFactory;
        $this->blaCollectionFactory = $blaCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataBlaFactory = $dataBlaFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Perspective\Yest\Api\Data\BlaInterface $bla
    ) {
        /* if (empty($bla->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $bla->setStoreId($storeId);
        } */
        
        $blaData = $this->extensibleDataObjectConverter->toNestedArray(
            $bla,
            [],
            \Perspective\Yest\Api\Data\BlaInterface::class
        );
        
        $blaModel = $this->blaFactory->create()->setData($blaData);
        
        try {
            $this->resource->save($blaModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the bla: %1',
                $exception->getMessage()
            ));
        }
        return $blaModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($blaId)
    {
        $bla = $this->blaFactory->create();
        $this->resource->load($bla, $blaId);
        if (!$bla->getId()) {
            throw new NoSuchEntityException(__('Bla with id "%1" does not exist.', $blaId));
        }
        return $bla->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->blaCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Perspective\Yest\Api\Data\BlaInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Perspective\Yest\Api\Data\BlaInterface $bla
    ) {
        try {
            $blaModel = $this->blaFactory->create();
            $this->resource->load($blaModel, $bla->getBlaId());
            $this->resource->delete($blaModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Bla: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($blaId)
    {
        return $this->delete($this->get($blaId));
    }
}

