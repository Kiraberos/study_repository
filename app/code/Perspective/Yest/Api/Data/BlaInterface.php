<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Api\Data;

interface BlaInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const BLA = 'Bla';
    const BLA_ID = 'bla_id';

    /**
     * Get bla_id
     * @return string|null
     */
    public function getBlaId();

    /**
     * Set bla_id
     * @param string $blaId
     * @return \Perspective\Yest\Api\Data\BlaInterface
     */
    public function setBlaId($blaId);

    /**
     * Get Bla
     * @return string|null
     */
    public function getBla();

    /**
     * Set Bla
     * @param string $bla
     * @return \Perspective\Yest\Api\Data\BlaInterface
     */
    public function setBla($bla);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Perspective\Yest\Api\Data\BlaExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Perspective\Yest\Api\Data\BlaExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Perspective\Yest\Api\Data\BlaExtensionInterface $extensionAttributes
    );
}

