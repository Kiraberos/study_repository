<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Api\Data;

interface BlaSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{

    /**
     * Get Bla list.
     * @return \Perspective\Yest\Api\Data\BlaInterface[]
     */
    public function getItems();

    /**
     * Set Bla list.
     * @param \Perspective\Yest\Api\Data\BlaInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}

