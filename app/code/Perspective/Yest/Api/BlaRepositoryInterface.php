<?php
/**
 * Copyright ©  All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Perspective\Yest\Api;

use Magento\Framework\Api\SearchCriteriaInterface;

interface BlaRepositoryInterface
{

    /**
     * Save Bla
     * @param \Perspective\Yest\Api\Data\BlaInterface $bla
     * @return \Perspective\Yest\Api\Data\BlaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function save(
        \Perspective\Yest\Api\Data\BlaInterface $bla
    );

    /**
     * Retrieve Bla
     * @param string $blaId
     * @return \Perspective\Yest\Api\Data\BlaInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function get($blaId);

    /**
     * Retrieve Bla matching the specified criteria.
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Perspective\Yest\Api\Data\BlaSearchResultsInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
    );

    /**
     * Delete Bla
     * @param \Perspective\Yest\Api\Data\BlaInterface $bla
     * @return bool true on success
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(
        \Perspective\Yest\Api\Data\BlaInterface $bla
    );

    /**
     * Delete Bla by ID
     * @param string $blaId
     * @return bool true on success
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($blaId);
}

