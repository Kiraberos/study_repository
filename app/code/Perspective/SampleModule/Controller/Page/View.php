<?php
namespace Perspective\SampleModule\Controller\Page;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\ForwardFactory;
use Magento\Framework\View\Result\PageFactory;
use Perspective\SampleModule\Helper\Data;

class View extends Action
{
    protected $helperData;

    protected $forwardFactory;

    protected $_pageFactory;
    /**
     * @var ForwardFactory
     */
    private $_forwardFactory;

    public function __construct(
        Data $helperData,
        ForwardFactory $forwardFactory,
        PageFactory $pageFactory,
        Context $context
    ) {
        $this->helperData = $helperData;
        $this->_forwardFactory = $forwardFactory;
        $this->_pageFactory = $pageFactory;
        return parent::__construct($context);
    }

    public function execute()
    {
        if ($this->helperData->getGeneralConfig('enable')) {
            $this->_view->loadLayout();
            $this->_view->renderLayout();
        } else {
            $resultForward = $this->_forwardFactory->create();
            $resultForward->setController('index');
            $resultForward->forward('defaultNoRoute');
            return $resultForward;
        }
    }
}
