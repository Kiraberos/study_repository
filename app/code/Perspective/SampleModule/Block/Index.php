<?php

namespace Perspective\SampleModule\Block;

use Magento\Framework\DataObject;

class Index extends \Magento\Framework\View\Element\Template
{

    protected $helperData;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Perspective\SampleModule\Helper\Data $helperData
    ) {
        $this->helperData = $helperData;
        return parent::__construct($context);
    }

    public function getObserverText()
    {
//        return $this->helperData->getGeneralConfig('display_text');
         $textDisplay = new DataObject(['text' => 'Magento']);
         $this->_eventManager->dispatch('helloworld_display_text', ['mp_text' => $textDisplay]);
         echo $textDisplay->getText();
         echo $this->helperData->getGeneralConfig('enable');
         echo $this->helperData->getGeneralConfig('display_text');
    }
}
