require([
    'jquery'
], function ($) {
    'use strict';

    $(document).ready(function () {
        var input_class = "required-entry maximum-length-255 minimum-length-1 validate-length input-text admin__control-text required-entry _required";
        var input_id = $('#order-billing_address_middlename, #order-shipping_address_middlename');
        $('#order-shipping_method').on('click', 'input', function () {
            if ($('#s_method_freeshipping_freeshipping').attr('checked') === 'checked') {
                input_id.parent().parent().addClass(input_class);
                input_id.addClass(input_class);
            } else {
                input_id.removeClass('required-entry _required');
                input_id.parent().parent().removeClass(input_class);
            }

            // var class_input_edit = "required-entry maximum-length-255 minimum-length-1 validate-length required-entry _required";
            // var selector_input = $("#order-billing_address_middlename, #order-shipping_address_middlename");
            // var selector_input_shipping = $("div .field-middlename");
            // if ($("#s_method_freeshipping_freeshipping").attr("checked") == 'checked') {
            //     selector_input_shipping.addClass('field-middlename _required');
            //     selector_input.addClass(class_input_edit);
            // } else {
            //     selector_input_shipping.removeClass('_required');
            //     selector_input.removeClass(class_input_edit);
            // }
        });
    });
});
