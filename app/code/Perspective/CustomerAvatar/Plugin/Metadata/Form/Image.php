<?php

namespace Perspective\CustomerAvatar\Plugin\Metadata\Form;

use Magento\Framework\Exception\LocalizedException;

class Image
{
    protected $validImage;

    public function __construct(
        \Perspective\CustomerAvatar\Model\Source\Validation\Image $validImage
    ) {
        $this->validImage = $validImage;
    }

    /**
     * @param \Magento\Customer\Model\Metadata\Form\Image $subject
     * @param $value
     * @return array
     * @throws LocalizedException
     */
    public function beforeExtractValue(\Magento\Customer\Model\Metadata\Form\Image $subject, $value)
    {
        $attrCode = $subject->getAttribute()->getAttributeCode();

        if ($this->validImage->isImageValid('tmp_name', $attrCode) === false) {
            $_FILES[$attrCode]['tmpp_name'] = $_FILES[$attrCode]['tmp_name'];
            unset($_FILES[$attrCode]['tmp_name']);
        }

        return [];
    }
}
