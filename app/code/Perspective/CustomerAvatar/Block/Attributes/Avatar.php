<?php

namespace Perspective\CustomerAvatar\Block\Attributes;

use Magento\Customer\Api\CustomerMetadataInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Customer\Model\ResourceModel\Customer;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\View\Asset\Repository;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\MediaStorage\Helper\File\Storage;

class Avatar extends Template
{
    protected $viewFileUrl;
    /**
     * @var Storage
     */
    private $storage;
    /**
     * @var Filesystem
     */
    private $fileSystem;
    /**
     * @var CustomerFactory
     */
    private $customerFactory;
    /**
     * @var Customer
     */
    private $customerResource;

    /**
     * Avatar constructor.
     * @param Context $context
     * @param Repository $viewFileUrl
     * @param CustomerFactory $customerFactory
     * @param Storage $storage
     * @param Filesystem $filesystem
     * @param Customer $customerResource
     */
    public function __construct(
        Context $context,
        Repository $viewFileUrl,
        CustomerFactory $customerFactory,
        Storage $storage,
        Filesystem $filesystem,
        Customer $customerResource
    ) {
        $this->customerResourceModel = $customerResource;
        $this->fileSystem = $filesystem;
        $this->storage = $storage;
        $this->viewFileUrl = $viewFileUrl;
        $this->customerFactory = $customerFactory;
        parent::__construct($context);
    }

    /**
     * @param $file
     * @return bool
     */
    public function checkImageFile($file)
    {
        $file = base64_decode($file);
        $directory = $this->fileSystem->getDirectoryRead(DirectoryList::MEDIA);
        $fileName = CustomerMetadataInterface::ENTITY_TYPE_CUSTOMER . '/' . ltrim($file, '/');
        $path = $directory->getAbsolutePath($fileName);
        if (!$directory->isFile($fileName)
            && !$this->storage->processStorageFile($path)
        ) {
            return false;
        }
        return true;
    }

    /**
     * @param $file
     * @return string
     */
    public function getAvatarCurrentCustomer($file)
    {
        if ($this->checkImageFile(base64_encode($file)) === true) {
            return $this->getUrl('viewfile/avatar/view/', ['image' => base64_encode($file)]);
        }
        return $this->viewFileUrl->getUrl('Perspective_CustomerAvatar::images/no-profile-photo.jpg');
    }

    /**
     * Get the avatar of the customer by the customer id
     * @return string
     */
    public function getCustomerAvatarById($customerId = false)
    {
        if ($customerId) {
            $customerModel = $this->customerFactory->create();
            $this->customerResource->load($customerModel, $customerId);

            if ($customerModel && !empty($customerModel->getProfilePicture())) {
                if ($this->checkImageFile(base64_encode($customerModel->getProfilePicture())) === true) {
                    return $this->getUrl(
                        'viewfile/avatar/view/',
                        [
                        'image' => base64_encode($customerModel->getProfilePicture())]
                    );
                }
            }
        }
        return $this->viewFileUrl->getUrl('Perspective_CustomerAvatar::images/no-profile-photo.jpg');
    }
}
