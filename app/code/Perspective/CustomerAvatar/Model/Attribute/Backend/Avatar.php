<?php

namespace Perspective\CustomerAvatar\Model\Attribute\Backend;

use Magento\Eav\Model\Entity\Attribute\Backend\AbstractBackend;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Perspective\CustomerAvatar\Model\Source\Validation\Image;

class Avatar extends AbstractBackend
{
    /**
     * @param DataObject $object
     * @return Avatar
     * @throws LocalizedException
     */
    public function beforeSave($object)
    {
        $validation = new Image();
        $attrCode = $this->getAttribute()->getAttributeCode();
        if ($attrCode == 'customer_image') {
            if ($validation->isImageValid('tmpp_name', $attrCode) === false) {
                throw new LocalizedException(
                    __('The profile picture is not a valid image.')
                );
            }
        }

        return parent::beforeSave($object);
    }
}
