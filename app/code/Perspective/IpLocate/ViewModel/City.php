<?php

namespace Perspective\IpLocate\ViewModel;

use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Stdlib\ArrayManager;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Perspective\IpLocate\Helper\Data;

class City implements ArgumentInterface
{
    /**
     * @var SerializerInterface
     */
    private $serialize;
    /**
     * @var ArrayManager
     */
    private $arrayManager;
    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var Data
     */
    private $storage;

    public function __construct(
        SerializerInterface $serialize,
        ArrayManager $arrayManager,
        Curl $curl,
        Data $storage
    ) {
        $this->serialize = $serialize;
        $this->arrayManager = $arrayManager;
        $this->curl = $curl;
        $this->storage = $storage;
    }

    /**
     * @return mixed
     */
    protected function getCurrentCity()
    {
        if ($this->storage->getEnableConfig()){
            return $this->getCity();
        }
        return false;
    }

    /**
     * @return mixed|string|null
     */
    public function getCity()
    {
        $getAccessKey = $this->storage->getAccessKey();
        //$visitorIp =  $this->remoteAddress->getRemoteAddress();
        $visitorIp = '176.108.236.254';
        $url = "http://api.ipstack.com/" . $visitorIp . "?access_key=$getAccessKey" . "&language=ru";
        $this->curl->get($url);
        $response = $this->serialize->unserialize($this->curl->getBody());
        if ($this->arrayManager->exists('city', (array)$response) && $response['city']) {
            return $this->arrayManager->get('city', (array)$response);
        }
        return "Киев";
    }
}
