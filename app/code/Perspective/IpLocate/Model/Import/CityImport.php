<?php

namespace Perspective\IpLocate\Model\Import;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Perspective\IpLocate\Api\Data\CityInterface;
use Perspective\IpLocate\Model\CityFactory;
use Perspective\IpLocate\Model\ResourceModel\City\CollectionFactory;
use Perspective\IpLocate\Api\Data\CityInterfaceFactory;
use Perspective\IpLocate\Model\ResourceModel\City;
use Perspective\IpLocate\Service\Curl;

/**
 * Import cities from novaposhta.ua
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.CyclomaticComplexity)
 */
class CityImport
{

    /**
     * @var Curl
     */
    private $curl;
    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var CityFactory
     */
    private $cityFactory;

    /**
     * @var City
     */
    private $cityResource;

    /**
     * @var CollectionFactory
     */
    private $cityCollectionFactory;

    /**
     * @var CityInterfaceFactory
     */
    private $cityInterfaceFactory;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param Curl $curl
     * @param CityFactory $cityFactory
     * @param City $cityResource
     * @param CollectionFactory $cityCollectionFactory
     * @param CityInterfaceFactory $cityInterfaceFactory
     */
    public function __construct(
        ScopeConfigInterface $scopeConfig,
        Curl $curl,
        CityFactory $cityFactory,
        City $cityResource,
        CollectionFactory $cityCollectionFactory,
        CityInterfaceFactory $cityInterfaceFactory
    ) {
        $this->curl = $curl;
        $this->scopeConfig = $scopeConfig;
        $this->cityFactory = $cityFactory;
        $this->cityResource = $cityResource;
        $this->cityCollectionFactory = $cityCollectionFactory;
        $this->cityInterfaceFactory = $cityInterfaceFactory;
    }

    /**
     * @SuppressWarnings(PHPMD.ShortVariable)
     * @param \Closure $cl
     * @return void
     */
    public function execute(\Closure $cl = null)
    {
        $citiesFromNovaPoshta = $this->importCities();
        if ($citiesFromNovaPoshta == null) {
            if (is_callable($cl)) {
                $cl('Ошибка импорта городов. Проверьте ключ API.');
                return ;
            }
        }

        $cities = $this->getCitiesFromDb();

        foreach ($citiesFromNovaPoshta as $cityFromNovaPoshta) {
            $key = array_search($cityFromNovaPoshta['ref'], array_column($cities, 'ref'), true);

            if ($key === false || empty($key)) {
                $this->saveCity($cityFromNovaPoshta);
            } elseif (isset($cities[$key]['city_id'])) {

                if (($cities[$key]['ref'] !== $cityFromNovaPoshta['ref']) ||
                        ($cities[$key]['name_ua'] !== $cityFromNovaPoshta['name_ua']) ||
                        ($cities[$key]['name_ru'] !== $cityFromNovaPoshta['name_ru']) ||
                        ($cities[$key]['area'] !== $cityFromNovaPoshta['area']) ||
                        ($cities[$key]['type_ua'] !== $cityFromNovaPoshta['type_ua']) ||
                        ($cities[$key]['type_ru'] !== $cityFromNovaPoshta['type_ru'])
                ) {
                    $cityId = $cities[$key]['city_id'];
                    $this->saveCity($cityFromNovaPoshta, $cityId);
                }
            }

            if (is_callable($cl)) {
                $cl($cityFromNovaPoshta['ref'] . ' ' . $cityFromNovaPoshta['name_ru']);
            }
        }
    }

    /**
     * @param array<mixed> $data
     * @param int|null $cityId
     * @throws \Magento\Framework\Exception\AlreadyExistsException
     */
    private function saveCity(array $data, $cityId = null)
    {
        $cityModel = $this->cityFactory->create();
        $cityModel->setCityId($cityId);
        $cityModel->setRef($data['ref']);
        $cityModel->setNameUa(($data['name_ua'] ? $data['name_ru'] : $data['']));
        $cityModel->setNameRu(($data['name_ru'] ? $data['name_ua'] : $data['']));
        $cityModel->setArea($data['area']);
        $cityModel->setTypeUa($data['type_ua']);
        $cityModel->setTypeRu($data['type_ru']);
        $this->cityResource->save($cityModel);
    }

    /**
     * Return cities array
     *
     * @return array<mixed>
     */
    private function getCitiesFromDb(): array
    {
        $cityCollection = $this->cityCollectionFactory->create();

        $data = $cityCollection->load()->toArray();
        return $data['items'];
    }

    /**
     * @return string[]
     */
    private function prepareRequestParams(): array
    {
        return [
            'modelName' => 'Address',
            'calledMethod' => 'getStreet',
        ];
    }

    /**
     * @return array<mixed> | null
     */
    private function importCities(): ?array
    {
        $params = $this->prepareRequestParams();

        $cityData = [];
        $data = $this->curl->getDataImport($params);

        if (!$data) {
            return $cityData;
        }

        foreach ($data as $datum) {
            /** @var CityInterface $street */
            $city = $this->cityInterfaceFactory->create();
            $cityData[] = [
                'ref'     => $datum['Ref'],
                'name_ua' => $datum['Description'],
                'name_ru' => $datum['DescriptionRu'],
                'area'    => $datum['Area'] ?? '',
                'type_ua' => $datum['SettlementTypeDescription'] ?? '',
                'type_ru' => $datum['SettlementTypeDescriptionRu'] ?? '',
            ];
        }
            return $cityData;
    }
}
