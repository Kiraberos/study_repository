<?php

namespace Perspective\CustomPrice\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{

    const XML_PATH_CUSTOM_PATH = 'perspective_custom_price_section_id/perspective_price_group_id/custom_price_discount';

    public function getConfigValue($field, $storeId = null)
    {
        return $this->scopeConfig->getValue(
            $field,
            ScopeInterface::SCOPE_STORE,
            $storeId
        );
    }
    public function getDiscountCustomPrice()
    {
        return $this->getConfigValue(self::XML_PATH_CUSTOM_PATH);
    }
}
